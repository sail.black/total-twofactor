#The PAM configuration file for the Shadow ‘login’ service
auth optional pam_faildelay.so delay=3000000
auth [success=ok new_authtok_reqd=ok ignore=ignore user_unknown=bad default=die] pam_securetty.so
auth requisite pam_nologin.so
session [success=ok ignore=ignore module_unknown=ignore default=bad] pam_selinux.so close
session required pam_loginuid.so
session [success=ok ignore=ignore module_unknown=ignore default=bad] pam_selinux.so open
#parsing /etc/environment needs “readenv=1”
session required pam_env.so readenv=1
session required pam_env.so readenv=1 envfile=/etc/default/locale
#Standard Un*x authentication.
@include common-auth
session optional pam_motd.so noupdate
#MODIFICATION: required for Google Authenticator 2FA
auth required pam_google_authenticator.so
#This allows certain extra groups to be granted to a user
auth optional pam_group.so
#Sets up user limits according to /etc/security/limits.conf
session required pam_limits.so
#Prints the last login info upon successful login
session optional pam_lastlog.so
#Prints the message of the day upon successful login.
session optional pam_motd.so motd=/run/motd.dynamic
session optional pam_motd.so noupdate
#
session optional pam_mail.so standard
#Create a new session keyring.
session optional pam_keyinit.so force revoke
#Standard Un*x account and session
@include common-account
@include common-session
@include common-password