# total-twofactor

A repo walking you through the process of setting up 2FA on Debian
## Secure Desktop and SSH with 2FA 

### SMS 

When I read about SMS authenticator, I realized it is not an authentication method that is really a 2FA
Basically, it seems effortless to intercept an SMS authentication. 


General thoughts about SMS authenticator [blog](https://stephenreescarter.net/the-security-risk-of-sms-two-factor-authentication/)

Reroute SMS [blog](https://www.vice.com/en/article/y3g8wb/hacker-got-my-texts-16-dollars-sakari-netnumber)

### WebAuthN

There is an attempt to secure more users on the Web with WebAuthN.
https://w3c.github.io/webauthn/


### TOTP 

It is not 100% secure, but people will have a hard time fooling you to reveal your codes two times. But if they do, you are screwed. Still, I consider the risk more apparent for web applications. The attack goes through phishing your codes.
### Installation of Google-Auth 

` sudo apt install libpam-google-authenticator`

### Secure User

Sign in the user who should be secured. In the shell, run:

`google-authenticator`

To secure all pam operations you have to edit `/etc/pam.d/common-auth` on Debian or the equivalent on other systems

Insert 

```bash
auth required pam_google_authenticator.so
```

### Walk through the program 

Answer all questions with `y`. Then, delete shell history with 

`history -c`

`clear`

### Change the login files 

The files are given in the repo. 

`sudo rm /etc/pam.d/login`
`sudo cp login /etc/pam.d/`

Identify the desktop environment (e.g. on UBUNTU)

`sudo rm /etc/pam.d/gdm-password`
`sudo cp gdm-password /etc/pam.d/`

On MATE desktop environment (Linux Mint and Parrot OS), you need to replace the `lightdm` file with the contents of the gdm-password file. 

`sudo rm /etc/pam.d/sshd`
`sudo cp sshd  /etc/pam.d/`


## Check history of logins 

Use the `last` command
Three log files

* /var/log/wtmp – Logs of last login sessions
* /var/run/utmp – Logs of the current login sessions
* /var/log/btmp – Logs of the bad login attempts


